<?php

if (isset($_POST["envoie"])){
    require_once "./connection.php";
    require_once "./vendor/autoload.php";
    $myReq = $connexion->query("SELECT * from emps");
    $employes = $myReq->fetchAll(PDO::FETCH_OBJ);

    $sql = "SELECT * from emps";
    $stmt = $connexion->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
    $stmt->execute();

    if (mb_strlen($_POST["subject"])==0){

    }else if(mb_strlen($_POST["email"])==0){

    }else{
        $contenu ="";
        $tableHead = "<table border='1'><thead><th>N°</th><th>Nom</th><th>Prénom</th></thead><tbody>";
        $tableFoot="</tbody></table>";

            while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
                $contenu = "<tr><td>". $row[0]. "</td><td> ".$row[1]. "</td> <td>".$row[2]. "</td></tr>". $contenu;
           }

        // Create the Transport
        $transport = (new Swift_SmtpTransport("localhost", 1025))
            ->setUsername("")
            ->setPassword("");


}
        // Create the Mailer using your created Transport

        $mailer = new Swift_Mailer($transport);
        $message = (new Swift_Message($_POST["subject"]))
            ->setFrom(['mombesoft@gmail.com' => 'Mombesoft'])
            ->setTo([$_POST["email"] => 'Email Destionation'])
            ->setBody($tableHead." ".$contenu." ".$tableFoot, "Text/Html");
        // Send the message
        $result = $mailer->send($message);
        header("location:index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>MTN</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="MDB/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="MDB/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="MDB/css/style.css" rel="stylesheet">
</head>

<body>

<!-- Start your project here-->
<br>
<br>
<div class="container">
    <!--Table-->
    <div class="row">
        <div class="col-4 offset-4">
            <!-- Default form contact -->
            <!-- Card -->
            <div class="card">

                <!-- Card body -->
                <div class="card-body">

                    <!-- Default form subscription -->
                    <form method="post" >
                        <p class="h4 text-center py-4">Envoi des données via email.</p>

                        <!-- Default input name -->
                        <label for="defaultFormCardNameEx" class="grey-text font-weight-light">Sujet</label>
                        <input name="subject" type="text" id="defaultFormCardNameEx" class="form-control">

                        <br>

                        <!-- Default input email -->
                        <label for="defaultFormCardEmailEx" class="grey-text font-weight-light">Email de destination</label>
                        <input name="email" type="email" id="defaultFormCardEmailEx" class="form-control">

                        <div class="text-center py-4 mt-3">
                            <button class="btn btn-outline-yellow"  name="envoie" type="submit">Send<i class="fa fa-paper-plane-o ml-2"></i></button>
                        </div>
                    </form>
                    <!-- Default form subscription -->

                </div>
                <!-- Card body -->

            </div>
            <!-- Card -->
            <!-- Default form contact -->
        </div>
    </div>
    <!--Table-->
</div>

<!-- /Start your project here-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="MDB/js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="MDB/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="MDB/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="MDB/js/mdb.min.js"></script>
</body>

</html>


