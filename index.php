<?php
    require_once "./connection.php";
try
{
    // connexion à la base Oracle et création de l'objet
    $connexion = new PDO($lien_base, $utilisateur, $motdepasse);
    //echo "Connexion reuissi";
}
catch (PDOException $erreur)
{
    echo $erreur->getMessage();
}

$myReq = $connexion->query("SELECT * from emps");
$employes = $myReq->fetchAll(PDO::FETCH_OBJ);

$sql = "SELECT * from emps";
$stmt = $connexion->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
$stmt->execute();


/*var_dump($employes);
die();*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>MTN</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="MDB/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="MDB/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="MDB/css/style.css" rel="stylesheet">
</head>

<body>

<!-- Start your project here-->
<br>
<br>
<div class="container">
    <!--Table-->
    <a href="mail.php" class="btn btn-primary fa fa-envelop" >Envoyer un email.</a>
    <table class="table">

        <!--Table head-->
        <thead class=" yellow">
        <tr class="text-white">
            <th>#</th>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Fonction</th>
        </tr>
        </thead>
        <tbody>
        <?php
            while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) { ?>
               <tr>
                   <td><?= $row[0] ?></td>
                   <td><?= $row[1] ?></td>
                   <td><?= $row[2] ?></td>
                   <td><?= $row[3] ?></td>
               </tr>
          <?php  }
        $stmt = null;
        ?>
        </tbody>
    </table>
    <!--Table-->
</div>

<!-- /Start your project here-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="MDB/js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="MDB/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="MDB/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="MDB/js/mdb.min.js"></script>
</body>

</html>


